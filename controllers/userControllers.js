const User = require("../models/User");

const bcrypt = require("bcrypt");

const auth = require("../auth");

// User Registration:
module.exports.registerUser = (req,res) =>{
	e.preventDefault();

	const hashedPw = bcrypt.hashSync(req.body.password,6);
	console.log(hashedPw);

	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPw,
		mobileNo: req.body.mobileNo,
		isAdmin: req.body.isAdmin
		
	});
	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
} 

// User Authentication:
module.exports.loginUser = (req,res) => {
	console.log(req.body);

	User.findOne({email:req.body.email})
	.then(foundUser => {
		if(foundUser === null){
			return res.send(false)
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password)
			if(isPasswordCorrect){
				console.log(foundUser)
						return res.send({accessToken: auth.createAccessToken(foundUser)});
					} else{

						return res.send(false);

					}
				}
		})
		.catch(err => res.send(err));
	}
	
// Get User Details
module.exports.getUserDetails = (req,res)=>{

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}




// Check User Email
module.exports.checkEmailExists = (req,res) => {

	User.findOne({email: req.body.email})
	.then(result => {

		//console.log(result)

		if(result === null){
			return res.send(false);
		} else {
			return res.send(true)
		}

	})
	.catch(err => res.send(err));
}


// Set user as admin (Admin Only)
module.exports.updateUser = (req,res) => {
	   let update = {

	        isAdmin: true

	    }

	    User.findByIdAndUpdate(req.params.userId,update,{
	        new:true})
	    .then(result => res.send(result))
	    .catch(error => res.send(error))
	};

