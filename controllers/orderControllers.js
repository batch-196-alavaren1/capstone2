const Order = require("../models/Order");


// Non-admin User checkout (Create Order):
module.exports.createOrder = (req,res) => {
 if(req.user.isAdmin){
 	return  res.send({message: "Successful!"});
 }

let newOrder = new Order({
	totalAmount: req.body.totalAmount,
	userId: req.user.id,
	products: [
		{
			productId: req.body.productId,
			quantity: req.body.quantity,
		},
	],
	
});


newOrder.save()
	.then(result => res.send({message: "Order Successful!"}))
	.catch(error => res.send())
};

// Authenticated user's order
module.exports.getOrders = (req,res)=>{
   
 	// console.log(req.params)
 	// console.log(req.user.id)

    Order.find({userId: req.user.id})
    .then(result => res.send(result))
    .catch(error => res.send(error))
    
};

// All Orders from All Users (Admin Only)
module.exports.getAllOrders = (req,res)=>{

	Order.find(req.body.order)
	.then(result => res.send(result))
	.catch(error => res.send())
	
};