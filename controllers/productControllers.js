const Product = require("../models/Product");


// Retrieve all active products:
module.exports.getAllProducts = (req,res)=>{

	Product.find({isActive:true})
	.then(result=> res.send(result))
	.catch(error=> res.send(error))


}

// Retrieve all active products avail = true:
module.exports.getAllProducts_Avail = (req,res)=>{

	Product.find({isActive:true,
				 isAvail:true })
	.then(result=> res.send(result))
	.catch(error=> res.send(error))


}



// Retrieve single product:
module.exports.getProduct = (req,res)=>{

	Product.findById(req.params.productId)
	.then(result=> res.send(result))
	.catch(error=> res.send(error))
}



// Create Product(Admin Only):
module.exports.addProduct = (req,res)=>{
	let newProduct = new Product({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		isActive: req.body.isActive
		
	})

	newProduct.save()
		.then(result => res.send(result))
		.catch(error => res.send(error))
};

// Update Product Information (Admin Only):
module.exports.updateProduct = (req,res)=>{
    let update = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    }
    Product.findByIdAndUpdate(req.params.productId,update,{
        new:true})
    .then(result => res.send(result))
    .catch(error => res.send(error))
};

// Delete Product(Admin Only):
module.exports.deleteProduct = (req,res) => {


    let update = {

        isActive: false
    }
    
    Product.findByIdAndUpdate(req.params.productId,update,{new:true})
    .then(result => res.send(result))
    .catch(error => res.send(error))
}

//Archive product
module.exports.archiveProduct = (req, res) => {

	let archive = {

		isAvail: false
	}

	Product.findByIdAndUpdate(req.params.productId,archive,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

//Unarchive product or Activate product
module.exports.activateProduct = (req, res) => {

	let unarchive = {

		isAvail: true
	}

	Product.findByIdAndUpdate(req.params.productId,unarchive,{new:false})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

