const express = require("express");
const router = express.Router();
const auth = require("../auth");
const { verify,verifyAdmin } = auth;

const orderControllers = require("../controllers/orderControllers");

// Non-admin User checkout (Create Order):
router.post("/",verify,orderControllers.createOrder); 

// Authenticated user's order
router.get("/userOrders",verify,orderControllers.getOrders);

// All Orders from All Users (Admin Only)
router.get("/allOrders/",verify,verifyAdmin,orderControllers.getAllOrders);

module.exports = router;