const express = require("express");
const router = express.Router();
const auth = require("../auth");
const { verify,verifyAdmin } = auth;

const productControllers = require("../controllers/productControllers");

// Retrieve all active products:
router.get('/activeProducts', productControllers.getAllProducts);



// Retrieve all active products:
router.get('/activeProducts_avail', productControllers.getAllProducts_Avail);


// Retrieve single product:
router.get('/product/:productId',productControllers.getProduct);

// Create Product(Admin Only):
router.post('/', verify, verifyAdmin, productControllers.addProduct);

// Update Product Information (Admin Only):
router.put('/update/:productId',verify,verifyAdmin,productControllers.updateProduct)

// Archive Product(Admin Only):
router.put("/archive/:productId",verify,verifyAdmin,productControllers.archiveProduct);

// Archive Product(Admin Only):
router.put("/activate/:productId",verify,verifyAdmin,productControllers.activateProduct);

// Archive Product(Admin Only):
router.delete("/delete/:productId",verify,verifyAdmin,productControllers.deleteProduct);


module.exports = router;