const express = require("express");
const router = express.Router();
const auth = require("../auth");
const { verify,verifyAdmin } = auth;

const userControllers = require("../controllers/userControllers");

// User Registration:
router.post("/",userControllers.registerUser);

// User Authentication:
router.post('/login',userControllers.loginUser);

// Get User Details
router.get('/details',verify,userControllers.getUserDetails);

// check if email exists
router.post('/checkEmailExists',userControllers.checkEmailExists);

// Set user as admin (Admin Only)
router.put('/updateUser/:userId',verify,verifyAdmin,userControllers.updateUser)

module.exports = router;
